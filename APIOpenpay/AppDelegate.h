//
//  AppDelegate.h
//  APIOpenpay
//
//  Created by Edison Effect on 5/23/18.
//  Copyright © 2018 Edison Effect. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

