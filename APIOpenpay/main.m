//
//  main.m
//  APIOpenpay
//
//  Created by Edison Effect on 5/23/18.
//  Copyright © 2018 Edison Effect. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
