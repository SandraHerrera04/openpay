//
//  SearchCustomerViewController.h
//  APIOpenpay
//
//  Created by Edison Effect on 5/30/18.
//  Copyright © 2018 Edison Effect. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Services.h"
#import "CustomerViewController.h"
#import "ViewController.h"

@interface SearchCustomerViewController : UIViewController<ServicesDelegate>
{
    IBOutlet UILabel *labelCustomerId;

    Services *auxServices;
}
@property (strong, nonatomic) IBOutlet UITextField *textFieldCustomerId;
- (IBAction)buttonSearchCustomer:(id)sender;
- (IBAction)buttonSearchCards:(id)sender;
- (IBAction)buttonSaveCards:(id)sender;


@end
