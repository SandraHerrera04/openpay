//
//  OPCustomer.h
//  APIOpenpay
//
//  Created by Edison Effect on 5/25/18.
//  Copyright © 2018 Edison Effect. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OPAddress.h"
#import "OPStore.h"

@interface OPCustomer : NSObject

@property NSString *id;
@property NSString *external_id;

@property NSDate *creation_date;

@property NSString *name;

@property NSString *last_name;

@property NSString *email;

@property int phone_number;

@property NSString *status;

@property float balance;

@property int CLABE;

@property OPAddress *address;

@property OPStore *store;

@property BOOL requires_account;

-(void)getListOfCustomers;
@end
