//
//  ViewController.m
//  APIOpenpay
//
//  Created by Edison Effect on 5/23/18.
//  Copyright © 2018 Edison Effect. All rights reserved.
//

#import "ViewController.h"
#import "Openpay.h"
//                                                          Para usar valores definidos
#import "AcknowledgmentCodes.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"%@", _idCustomer);
    auxServices = [[Services alloc]init];
    auxServices.delegate =self;
    //                                                          Inicializa tipo de HEADER para el servicio
    auxServices.typeHeaders = ZDHeaderJsonBasicAuth;
    //                                                          Inicializa el usuario para autenticacion
    auxServices.username = @"sk_e283daf657924bd89b5a99fe6b5b3754";
    //                                                          Inicializa password para autenticacion
    auxServices.password = @"12345678";
    //                                                          Inicializa la dirección para llamar el servicio
    auxServices.serverName = SERVER_IP_OP;
    //                                                          Inicializa la dirección para llamar el servicio
    auxServices.serverName = SERVER_IP_OP;
    
   [auxServices sendGETWebServiceTo:ZDServiceListCusomers nameService: [ NSString stringWithFormat:@"%@/customers/%@",MERCHANT_ID, _idCustomer]];
    
//                                                          Para ocultar el teclado
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideKeyBoard)];
    [self.view addGestureRecognizer:tapGesture];
//                                                          creamos una instancia openpay
  _openPayAPI = [[Openpay alloc] initWithMerchantId:MERCHANT_ID apyKey:API_KEY isProductionMode:NO];
   _sessionId = [_openPayAPI createDeviceSessionId];
//    NSLog(@"create device ses %@",_sessionId);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) ChangeLabelsLanguage
{
    _textFieldCardNumber.text = NSLocalizedString(@"Card Number", nil);
}
                                                            #pragma mark -
                                                            #pragma mark ***** TapGesture *****
/**
 Sirve para ocultar el teclado cuando se termina de
 escribir en un textfield
 */
-(void) hideKeyBoard
{
    [_textFieldCardNumber resignFirstResponder];
    [_textFieldClientName resignFirstResponder];
    [_textFieldSecurityCode resignFirstResponder];
    [_textFieldExpirationYear resignFirstResponder];
    [_textFieldExpirationMonth resignFirstResponder];
}

                                                            #pragma mark -
                                                            #pragma mark ***** Buttons *****

- (void)error:(NSString *)strError service:(int)typeService code:(NSInteger)intCode
{
    
}
- (void)parserData:(NSDictionary *)dictionaryData service:(int)typeService
{
    _textFieldClientName.text = dictionaryData[@"name"];
    _textFieldClientName.enabled =NO;
}
                                                            #pragma mark -
                                                            #pragma mark ***** Buttons *****


//                                                          se crean tokens para capturar informacion de la tarjeta
-(void)createTokenWithCard
{

//                                                          Objeto tarjeta
    OPCard * card = [[OPCard alloc] init];

//                                                          Se especifica un cliente
    card.holderName = self.textFieldClientName.text;
//                                                          Numero de tarjeta
    card.number = self.textFieldCardNumber.text;
    card.expirationMonth = self.textFieldExpirationMonth.text;
    card.expirationYear = self.textFieldExpirationYear.text;
    card.cvv2 = self.textFieldSecurityCode.text;
    card.brand = @"Mastercard/Visa/American";

    
//                                                          objeto address para tarjeta
    OPAddress * address = [[OPAddress alloc] init];
    address.city = @"Queretaro";
    address.state = @"Queretaro";
    address.countryCode = @"MX";
    address.line1 = @"primera calle y numero";
    address.postalCode  = @"78340";
    address.line2 = @" Segunda direccion y numero interior";
    address.line3 = @"colonia";
//                                                          asignamos la direccion a la tarjeta
    card.address = address;
    
    
 //                                                         Creación de token para la tarjeta
    [_openPayAPI createTokenWithCard:card success:^(OPToken * token)
                {
//                                                          el token.id es el que se requerirá para realizar operaciones
                NSMutableDictionary *mutableDictionary = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                                          token.id, @"id",
                                                          _sessionId, @"device_session_id",
                                                          [token.card asMutableDictionary],@"card",
                                                          [card.address asMutableDictionary], @"Address",nil];
                 
//                                                          imprime el
                    NSLog(@"%@",mutableDictionary.description);
//                                                          obtiene la informacion de tarjeta con el token
                    [self getTokenResponse:token.id];
                    
                }
                failure:^(NSError * error)
                {
//                                                          se muestra descripcion del error y codigo
                 NSMutableDictionary *mutableDictionary = [[NSMutableDictionary alloc]initWithObjectsAndKeys:
                                                                                    [NSString stringWithFormat:@"%ld", (long) [error code]],
                                                           @"code", [error userInfo], @"info",nil];
                    NSLog(@"%@",mutableDictionary.description);
                }
    ];

    
}
- (IBAction)sendCardInfo:(id)sender {
//        [self createTokenWithCard];
    [self createDictionaryBodyForJSON];
    
}
-(void)createDictionaryBodyForJSON
{
    NSDictionary *dictionaryBody = [NSDictionary dictionaryWithObjectsAndKeys:
                                    //                                                          //Nombre del cliente.
                                    _textFieldCardNumber.text, @"card_number",
                                    //                                                          //Apellido del cliente.
                                   _textFieldClientName.text, @"holder_name",
                                    //                                                          //Email del cliente.
                                    _textFieldExpirationYear.text, @"expiration_year",
                                    //                                                          //Sin cuenta para manejo de saldo.
                                    _textFieldExpirationMonth.text, @"expiration_month",
                                    //                                                          //Número telefónico.
                                    _textFieldSecurityCode.text,@"cvv2",
                                    _sessionId, @"device_session_id",
                                    nil];
    [auxServices sendPOSTWebServiceTo:ZDServiceListCusomers nameService:[NSString stringWithFormat:@"mg5eramhegh7nboyvrrp/customers/%@/cards", _idCustomer] params:dictionaryBody];
}
/**
 Funcion para obtener datos de una tarjeta
 con el id de token

 @param variable <#variable description#>
 */
-(void) getTokenResponse :(NSString *)variable
{
        [_openPayAPI getTokenWithId:variable  success:^(OPToken *responseParams) {
            NSLog(@"Resp token %@ ",  responseParams.card.number);
            NSLog(@"Resp token %@ ",  responseParams.card.expirationYear);
            NSLog(@"Resp token %@ ",  responseParams.card.holderName);
            NSLog(@"Resp token %@ ",  responseParams.card.address.line1);
        } failure:^(NSError *error) {
            NSLog(@"Error desc %@ ", error.description);
        }];
}

@end
