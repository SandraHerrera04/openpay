//
//  CustomerViewController.m
//  APIOpenpay
//
//  Created by Edison Effect on 5/29/18.
//  Copyright © 2018 Edison Effect. All rights reserved.
//

#import "CustomerViewController.h"

@interface CustomerViewController ()

@end

@implementation CustomerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//                                                          Instancia de la clase servicios
    auxServices = [[Services alloc] init];
    auxServices.delegate=self;
//                                                          Para ocultar el teclado
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideKeyBoard)];
    [self.view addGestureRecognizer:tapGesture];
//                                                          Para cambiar el titulo
    self.navigationItem.title = @"Clientes";
//                                                          Inicializa tipo de HEADER para el servicio
    auxServices.typeHeaders = ZDHeaderJsonBasicAuth;
//                                                          Inicializa el usuario para autenticacion
    auxServices.username = @"sk_e283daf657924bd89b5a99fe6b5b3754";
//                                                          Inicializa password para autenticacion
    auxServices.password = @"12345678";
//                                                          Inicializa la dirección para llamar el servicio
    auxServices.serverName = SERVER_IP_OP;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



                                                            #pragma mark -
                                                            #pragma mark ***** Funciones *****
-(void) hideKeyBoard
{
    [_textFieldCustomerName resignFirstResponder];
    [_textFieldCustomerLastName resignFirstResponder];
    [_textFieldCustomerEmail resignFirstResponder];
    [_textFieldCustomerPhone resignFirstResponder];
}
                                                            #pragma mark -
                                                            #pragma mark ***** Alerts *****
-(void)alertError:(NSString *)strMessageError
{
    NSString *strTitle = NSLocalizedString(@"Warning", nil);
    NSString *strMessage = strMessageError;
    NSString *strButtonAccept = STR_OK;
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:strTitle message:strMessage preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:strButtonAccept style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                  {
                                      
                                  }];
    [alert addAction:firstAction];
    [self presentViewController:alert animated:YES completion:nil];
}
-(void)alertCustomerCreated
{
    NSString *strTitle = NSLocalizedString(@"Great", nil);
    NSString *strMessage = NSLocalizedString(@"Customer created successfully", nil);
    NSString *strButtonAccept = STR_OK;
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:strTitle message:strMessage preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:strButtonAccept style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                  {
                                      
                                  }];
    [alert addAction:firstAction];
    [self presentViewController:alert animated:YES completion:nil];
}
                                                            #pragma mark -
                                                            #pragma mark ***** ServiceDelegate *****
- (void)parserData:(NSDictionary *)dictionaryData service:(int)typeService
{
    NSLog(@" Delegado de servicio dictionary data: %@",dictionaryData);
    
    [self alertCustomerCreated];
    
}
- (void)error:(NSString *)strError service:(int)typeService code:(NSInteger)intCode
{

    [self validateIntCode:intCode messageError:strError];
}
-(void)validateIntCode: (NSInteger)intCode messageError:(NSString *)messageError
{
    switch (intCode) {
        case 1001:
            [self alertError:messageError];
            break;
        case 2003:
            [self alertError:NSLocalizedString(@"The customer with this external ID already exists", nil)];
            break;
        default:
            NSLog(@"No entró a ninguno de los casos");
            break;
    }
}
                                                            #pragma mark -
                                                            #pragma mark ***** Buttons *****

- (IBAction)buttonAddCustomer:(id)sender {
//                                                          Para crear un cliente
    [self createDictionaryBodyForJSON];

}

- (IBAction)buttonSearch:(id)sender {
//                                                          Buscar cliente
//        [auxServices sendGETWebServiceTo:ZDServiceListCusomers nameService:@"mg5eramhegh7nboyvrrp/customers/"];
}
-(void)createDictionaryBodyForJSON
{
    NSDictionary *dictionaryBody = [NSDictionary dictionaryWithObjectsAndKeys:
                                    //                                                          //Nombre del cliente.
                                    _textFieldCustomerName.text, @"name",
                                    //                                                          //Apellido del cliente.
                                    _textFieldCustomerLastName.text, @"last_name",
                                    //                                                          //Email del cliente.
                                    _textFieldCustomerEmail.text, @"email",
                                    //                                                          //Sin cuenta para manejo de saldo.
                                    @"false", @"requires_account",
                                    //                                                          //Número telefónico.
                                    _textFieldCustomerPhone.text,@"phone_number",
                                    @"1234509", @"external_id",
                                    nil];

    [auxServices sendPOSTWebServiceTo:ZDServiceListCusomers nameService:@"mg5eramhegh7nboyvrrp/customers/" params:dictionaryBody];
}

@end
