//
//  Customers.h
//  APIOpenpay
//
//  Created by Edison Effect on 5/25/18.
//  Copyright © 2018 Edison Effect. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OPCustomer.h"
#import "AcknowledgmentCodes.h"
#import "Services.h"



@interface Customers : UIViewController<UITextFieldDelegate>
{
    IBOutlet UILabel *labelCustomerName;
    
    IBOutlet UILabel *labelCustomerLastName;
    
    IBOutlet UILabel *labelCustomerEmail;
    
    IBOutlet UILabel *labelCustomerPhone;
    Services *auxServices;
    
}
@property (strong, nonatomic) IBOutlet UINavigationBar *navigationBar;
@property (strong, nonatomic) IBOutlet UITextField *textFieldCustomerName;
@property (strong, nonatomic) IBOutlet UITextField *textFieldCustomerLastName;
@property (strong, nonatomic) IBOutlet UITextField *textFieldCustomerEmail;
@property (strong, nonatomic) IBOutlet UITextField *textFieldCustomerPhone;

- (IBAction)buttonAddCustomer:(id)sender;


@end
