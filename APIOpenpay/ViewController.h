//
//  ViewController.h
//  APIOpenpay
//
//  Created by Edison Effect on 5/23/18.
//  Copyright © 2018 Edison Effect. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Openpay.h"
#import "Services.h"

@interface ViewController : UIViewController <UITextFieldDelegate,ServicesDelegate>

{
    
    IBOutlet UIButton *buttonSendCardInfo;
    IBOutlet UILabel *labelExpirationYear;
    IBOutlet UILabel *labelExpirationMonth;
    IBOutlet UILabel *labelSecurityNumber;
    IBOutlet UILabel *labelClientName;
    IBOutlet UILabel *labelCardNumber;
    Services *auxServices;
}

@property (strong, nonatomic) IBOutlet UITextField *textFieldClientName;
@property (strong, nonatomic) IBOutlet UITextField *textFieldCardNumber;
@property (strong, nonatomic) IBOutlet UITextField *textFieldSecurityCode;
@property (strong, nonatomic) IBOutlet UITextField *textFieldExpirationMonth;
@property (strong, nonatomic) IBOutlet UITextField *textFieldExpirationYear;

@property (nonatomic) Openpay * openPayAPI;
@property NSString *sessionId;
- (IBAction)sendCardInfo:(id)sender;
//                                                          VALORES QUE LLEGAN DE OTRA VISTA
//                                                          Es el id del cliente
@property(strong,nonatomic) NSString * idCustomer;
@end

