//
//  Customers.m
//  APIOpenpay
//
//  Created by Edison Effect on 5/25/18.
//  Copyright © 2018 Edison Effect. All rights reserved.
//

#import "Customers.h"

@interface Customers ()

@end

@implementation Customers

- (void)viewDidLoad
{
    [super viewDidLoad];
    auxServices = [[Services alloc] init];
//                                                          Para ocultar el teclado
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideKeyBoard)];
    [self.view addGestureRecognizer:tapGesture];
//                                                          Para cambiar el titulo
    self.navigationItem.title = @"Clientes";
//                                                          Borramos un cliente
//    [self deleteCustomer:@"atk07a8jqejf82zdupmd"];
//                                                          Obtenemos lista de clientes
//    [self getcustomers];
    
    auxServices.typeHeaders = ZDHeaderJsonBasicAuth;
    auxServices.username = @"sk_e283daf657924bd89b5a99fe6b5b3754";
    auxServices.password = @"12345678";
//    auxServices.serverName = SERVER_IP_OP;
    
    NSDictionary *dictionaryBody = [NSDictionary dictionaryWithObjectsAndKeys:
                                    //                  //Identificador del proyecto en este caso es el 2 (Agua).
                                    @"Gerardo", @"name",
                                    //                  //Saber que viene de la aplicación.
                                    @"gzam@levelgas.com", @"email",
                                    //                  //Parametros con que se le agregan de cada servicio.
                                    @"false", @"requires_account",
                                    nil];
    
//    [auxServices sendGETWebServiceTo:ZDServiceListCusomers nameService:@"mg5eramhegh7nboyvrrp/customers/"];
    [auxServices sendPOSTWebServiceTo:ZDServiceListCusomers nameService:@"mg5eramhegh7nboyvrrp/customers/" params:dictionaryBody];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)Add:(id)sender
{
    
   
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
                                                            #pragma mark -
                                                            #pragma mark ***** Funciones *****
-(void) hideKeyBoard
{
    [_textFieldCustomerName resignFirstResponder];
    [_textFieldCustomerLastName resignFirstResponder];
    [_textFieldCustomerEmail resignFirstResponder];
    [_textFieldCustomerPhone resignFirstResponder];
}


-(void)getcustomers
{
    NSString * strUrl = [NSString stringWithFormat:@"%@%@",SERVER_IP,@"mg5eramhegh7nboyvrrp/customers/"];
     NSURL *strURL = [NSURL URLWithString:strUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:strURL];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
//    NSString *authStr = [NSString stringWithFormat:@"%@:%@", @"sk_e283daf657924bd89b5a99fe6b5b3754", @"12345678"];
//    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [ NSString stringWithFormat:@"Basic c2tfZTI4M2RhZjY1NzkyNGJkODliNWE5OWZlNmI1YjM3NTQ6"];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithRequest:request
                completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                    if (!error) {
                        NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                        NSLog(@" respuesta %@",responseDictionary);
                    }
                    else
                    {
                        NSLog(@" error %@",error);
                    }
                }] resume];
}
-(void)deleteCustomer: (NSString *)idCustomer
{
    NSString * strUrl = [NSString stringWithFormat:@"%@%@%@",SERVER_IP,@"mg5eramhegh7nboyvrrp/customers/",idCustomer];
    NSURL *strURL = [NSURL URLWithString:strUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:strURL];
    [request setHTTPMethod:@"DELETE"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
//    NSString *authStr = [NSString stringWithFormat:@"%@:%@", @"sk_e283daf657924bd89b5a99fe6b5b3754", @"12345678"];
//    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [ NSString stringWithFormat:@"Basic c2tfZTI4M2RhZjY1NzkyNGJkODliNWE5OWZlNmI1YjM3NTQ6"];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithRequest:request
                completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                    if (!error) {
                        NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                        NSLog(@" respuesta %@",responseDictionary);
                    }
                    else
                    {
                        NSLog(@" error %@",error);
                    }
                }] resume];
}
-(void)addCustomer
{
    NSString *strUrl = @"https://sandbox-api.openpay.mx/v1/mg5eramhegh7nboyvrrp/customers";
    NSDictionary *jsonBodyDict = @{@"name":_textFieldCustomerName.text, @"last_name": _textFieldCustomerLastName.text, @"email": _textFieldCustomerEmail.text, @"phone_number": _textFieldCustomerPhone.text};
    NSLog(@"Dictionary json: %@",jsonBodyDict);
    NSData *jsonBodyData = [NSJSONSerialization dataWithJSONObject:jsonBodyDict options:kNilOptions error:nil];
    // watch out: error is nil here, but you never do that in production code. Do proper checks!
    
    NSMutableURLRequest *request = [NSMutableURLRequest new];
    request.HTTPMethod = @"POST";

    // for alternative 1:
    [request setURL:[NSURL URLWithString:strUrl]];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPBody:jsonBodyData];
    NSString *authValue = [ NSString stringWithFormat:@"Basic c2tfZTI4M2RhZjY1NzkyNGJkODliNWE5OWZlNmI1YjM3NTQ6"];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config
                                                          delegate:nil
                                                     delegateQueue:[NSOperationQueue mainQueue]];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:^(NSData * _Nullable data,
                                                                NSURLResponse * _Nullable response,
                                                                NSError * _Nullable error) {
                                                if (!error)
                                                
                                                {
                                                    NSHTTPURLResponse *asHTTPResponse = (NSHTTPURLResponse *) response;
                                                    NSLog(@"The response is: %@", asHTTPResponse.description);
                                                    // set a breakpoint on the last NSLog and investigate the response in the debugger
                                                   
                                                    // if you get data, you can inspect that, too. If it's JSON, do one of these:
                                                    NSDictionary *forJSONObject = [NSJSONSerialization JSONObjectWithData:data
                                                                                                                  options:kNilOptions
                                                                                                                    error:nil];
                                                    // or
                                                    NSArray *forJSONArray = [NSJSONSerialization JSONObjectWithData:data
                                                                                                            options:kNilOptions
                                                                                                              error:nil];
    
                                                    NSLog(@"One of these might exist - object: %@ \n array: %@", forJSONObject, forJSONArray);
                                                    NSInteger intCode = [forJSONObject[@"error_code"] integerValue];
                                                    if(intCode == 1001)
                                                    {
                                                        NSLog(@"Aquí se va mandar una alerta porque el usuario no puede dejar el email vacio");
                                                    }
                                                    NSLog(@"DAtos array: %@", forJSONObject);
                                                }
                                                else
                                                {
                                                    
                                                    NSLog(@"Error: %@",error.description);
                                                    
                                                }
                                                
                                            }];
    [task resume];
}
                                                            #pragma mark -
                                                            #pragma mark ***** Delegados services *****

- (IBAction)buttonAddCustomer:(id)sender {
    [self addCustomer];
}

- (IBAction)buttonSearch:(id)sender {
}

@end
