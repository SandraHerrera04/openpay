//
//  SearchCustomerViewController.m
//  APIOpenpay
//
//  Created by Edison Effect on 5/30/18.
//  Copyright © 2018 Edison Effect. All rights reserved.
//

#import "SearchCustomerViewController.h"

@interface SearchCustomerViewController ()

@end

@implementation SearchCustomerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    auxServices = [[Services alloc]init];

    
    
    auxServices.delegate =self;
    //                                                          Inicializa tipo de HEADER para el servicio
    auxServices.typeHeaders = ZDHeaderJsonBasicAuth;
    //                                                          Inicializa el usuario para autenticacion
    auxServices.username = @"sk_e283daf657924bd89b5a99fe6b5b3754";
    //                                                          Inicializa password para autenticacion
    auxServices.password = @"12345678";
    //                                                          Inicializa la dirección para llamar el servicio
    auxServices.serverName = SERVER_IP_OP;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

                                                            #pragma mark -
                                                            #pragma mark ***** Funciones ******
-(void)validateTextField:(int)bandera
{
    
    
    if ([_textFieldCustomerId hasText])
    {
        
        switch (bandera) {
            case 1:
                 NSLog(@"Opcion clientes");
                [auxServices sendGETWebServiceTo:ZDServiceListCusomers nameService: [ NSString stringWithFormat:@"%@/customers/%@",MERCHANT_ID,_textFieldCustomerId.text]];
                break;
            case 2:
                 NSLog(@"Opcion tarjetas");
                [auxServices sendGETWebServiceTo:ZDServiceListCusomers nameService: [ NSString stringWithFormat:@"%@/customers/%@/cards",MERCHANT_ID,_textFieldCustomerId.text]];
                break;
                
            default:
                break;
        }
    }
    else
    {
        switch (bandera) {
            case 1:
                [self alertEmptyTextField: [NSString stringWithFormat:NSLocalizedString(@"You need introduce the ID to verify the customer", nil)]];
                break;
            case 2:
                [self alertEmptyTextField: [NSString stringWithFormat:NSLocalizedString(@"You need introduce the ID to verify the card", nil)]];
                break;
                
            default:
                break;
        }
        
    }
}

/**
  Valida el error y da una descripcion al usuario
  
  @param intCode <#intCode description#>
  @param messageError <#messageError description#>
  */
-(void)validateIntCode: (NSInteger)intCode messageError:(NSString *)messageError
{
    switch (intCode) {
        case 1005:
            [self alertError:messageError];
            break;
        case 2003:
            [self alertError:NSLocalizedString(@"The customer with this external ID already exists", nil)];
            break;
        default:
            
            NSLog(@"No entró a ninguno de los casos");
            break;
    }
}
-(void)goToViewRegisterCustomer
{
    CustomerViewController *viewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"storyBoardRegister"];
       [self.navigationController pushViewController:viewController animated:YES];
}
-(void)goToAddCard: (NSString *)IdCustomer
{
    ViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"addCardToCustomer"];
    [viewController setIdCustomer:IdCustomer];
    [self.navigationController pushViewController:viewController animated:YES];
    
}

                                                            #pragma mark -
                                                            #pragma mark ***** Alertas ******
/**
 Muestra descripcion del error
 
 @param strMessageError <#strMessageError description#>
 */
-(void)alertError:(NSString *)strMessageError
{
    NSString *strTitle = NSLocalizedString(@"Warning", nil);
    NSString *strMessage = [NSString stringWithFormat: @"%@ %@",strMessageError,NSLocalizedString(@"You need to register.", nil)];
    NSString *strButtonAccept = @"continuar";
    NSString * strButtonCancel = NSLocalizedString(@"Cancel", nil);
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:strTitle message:strMessage preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:strButtonAccept style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                  {
                                      [self goToViewRegisterCustomer];
                                  }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:strButtonCancel style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:firstAction];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
}


-(void)alertEmptyTextField:(NSString *)strMessage
{
    NSString *strTitle = NSLocalizedString(@"Warning", nil);
    NSString *strButtonAccept = @"Accept";
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:strTitle message:strMessage preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:strButtonAccept style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                  {
                                  }];
    [alert addAction:firstAction];
    [self presentViewController:alert animated:YES completion:nil];
}
                                                            #pragma mark -
                                                            #pragma mark ***** Service Delegate ******
- (void)error:(NSString *)strError service:(int)typeService code:(NSInteger)intCode
{
//                                                          alerta que se muestra al usuario con el error
    [self validateIntCode:intCode messageError:strError];
    NSLog(@"%@", strError);
}

/**
 Delegado de Services cuando hubo respuesta

 @param dictionaryData <#dictionaryData description#>
 @param typeService <#typeService description#>
 */
- (void)parserData:(NSDictionary *)dictionaryData service:(int)typeService
{
    NSLog(@" datos del customer :%@",dictionaryData);
    NSDictionary *dictionaryCustomers = dictionaryData;
    NSArray *dictionaryCustomer = dictionaryData;

}

                                                            #pragma mark -
                                                            #pragma mark ***** Buttons ******
- (IBAction)buttonSearchCustomer:(id)sender {

    [self validateTextField:1];
}

- (IBAction)buttonSearchCards:(id)sender {
    [self validateTextField:2];
}

- (IBAction)buttonSaveCards:(id)sender {
    if (_textFieldCustomerId.hasText)
    {
        [self goToAddCard:_textFieldCustomerId.text];
        
    }
    else
    {
        [self alertEmptyTextField:[NSString stringWithFormat:@"%@",NSLocalizedString(@"Id customer is necessary for assign card.",nil)]  ];
    }
    
}
@end
