//
//  AcknowledgmentCodes.h
//  TecnoLevel
//
//  Created by Gerardo Zamudio on 09/01/18.
//  Copyright © 2018 Edison Effect. All rights reserved.
//

#ifndef AcknowledgmentCodes_h
#define AcknowledgmentCodes_h
//                                                          //Id openPay
#define MERCHANT_ID @"mg5eramhegh7nboyvrrp"
//                                                          //Llave pública openpay
//#define API_KEY @"pk_779db188bc3f4938adaa6d5702137228"
#define API_KEY @"sk_e283daf657924bd89b5a99fe6b5b3754"
#define SERVER_IP @"levelgas.com"
#define SERVER_IP_OP @"https://sandbox-api.openpay.mx/v1/"
#define MERCHANT_ID @"mg5eramhegh7nboyvrrp"
//                                                          //      se envia en Body en JSON en el web service.
#define PROJECT_ID                                          @"2"
//                                                          //      es para saber que viene de la aplicación móvil.
#define REQUESTED_FROM                                      @"app"
//                                                          //ApiKey de Google para obtener el mapa y direcciones.
#define API_KEY_GOOGLE                                      @"AIzaSyCY43MkDj_gCxwhRg8RzbeAOrZ-FFV7fOQ"
#define STATUS_OK                                           200
//                                                          //Texto de Alertas
#define STR_OK                                              NSLocalizedString(@"OK", nil)

#endif /* AcknowledgmentCodes_h */

